#pragma once
// Abandon hope, all ye who enter.
#include "parser.h"

int truthset(int inputcount, int iteration);
int yyparse();

// BUFSIZ is declared in string.h. Yes, 8192 bytes is a lot of stack memory to allocate.
char buffer[BUFSIZ];
int result;
// TODO: Don't use externs both ways! That's probably a REALLY bad idea!
extern int buffer_index;
