# The Truth Tabulator.

This is a program written using Bison that generates a truth table for a boolean statement.
It supports:

* Arbitrary-length input names.
* Operator precedence.
* Constants (1 & 0).

Supported logical operators:

* AND (&, .).
* OR (+, |).
* NOT (!, ¬).
* XOR (^).
* NAND (?).

# The Code Is A Mess.
* It's C.
* I have externs both ways between the two translation-units.

# The inspiration.
Large parts of the parser are based on the mfcalc example in the bison documentation.
I'm assuming that's OK since the only people who will ever see this project are the ones I tell about it.

# Building.
CMake is bloated and Autotools seem like a waste of my time so...

1. (Optional) Ensure you have Bison installed (if you wish to re-build parser.tab.c).
1. Examine the Makefile so you can be sure I'm not giving you a virus.
1. Move to the folder containing this project and run the following command:
```bash
$ make
```
