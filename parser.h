#pragma once

// Channel your inner Java developer...
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>


// You look familiar...
extern int n_inputs;
extern int result;
extern char buffer[BUFSIZ];

// The node type for the linked list.
struct inputrec
{
    char *name;
    int value;
    struct inputrec *next;
};

// It's a wonder C99 or whatever it is doesn't already do this for you.
typedef struct inputrec inputrec;

// Hello there.
extern inputrec *sym_table;

int yylex(void);
void yyerror (char const*);


// Functions for adding and retrieving symbols in the linked list.
inputrec *putsym (char const *name);
inputrec *getsym (char const *name);

// My lazy coder function.
int getnumfrombuffer(void);
