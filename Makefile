CC = gcc
CFLAGS = -g
OUTPUT_OPTION = -MMD -MP -o $@

SOURCE = $(wildcard *.c)
OBJS = $(SOURCE:.c=.o)
DEPS = $(SOURCE:.c=.d)


tabulating: ${OBJS} parser.tab.c
	${CC} -lm -o $@ ${OBJS}

parser.tab.c: parser.y
	bison -Wall $^

-include ${DEPS}

clean:
	rm -f *.o *.d

tags:
	ctags -R .
