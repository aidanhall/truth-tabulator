#include "main.h"

int truthset(int inputcount, int iteration)
{
    // Work out what value to set this input to.
    int change_every = pow(2, inputcount);

    return (iteration/change_every)%2;
}

void printvals(inputrec* current)
{
    // Display the result and input values.
    printf("%d\t", result);
    while(current)
    {
	printf("%d\t", current->value);
	current = current->next;
    }
    printf("\n");
}

void printnames(inputrec* current)
{
    // Display the names of the inputs.
    // TODO: Allow the user to select the name of the output.
    printf("Q\t");
    while(current)
    {
	// TODO: Handling for input names wider than a tab.
	printf("%s\t", current->name);
	current = current->next;
    }
    putchar('\n');
}

void setvals(inputrec* current, int iteration)
{
    // Set the values of the inputs for the current line of the truth table.
    int inputcount = 0;
    while(current)
    {
	current->value = truthset(inputcount++, iteration);
	current = current->next;
    }
}
int main(void)
{
    int exitcode;

    // Properly format the statement for the parser:
    printf("Statement: ");
    scanf("%s", buffer);
    strcat(buffer, "\n");



    /* printf("\nThere were %d input variables in total.\n", n_inputs); */

    // First pass to get the inputs.
    exitcode = yyparse();

    // Reset the index of the input string. This must be done after each call to yyparse().
    // TODO: Make this unnecessary.
    buffer_index = 0;

    // We don't want to know about an expression with no inputs.
    if (n_inputs == 0)
	return 1;

    // Print the header and first row (all zeroes) of the table.
    printnames(sym_table);
    printvals(sym_table);

    // Generate the rest of the truth table.
    for (int iteration = 1, required_iterations = pow(2, n_inputs); iteration < required_iterations; ++iteration)
    {
	setvals(sym_table, iteration);
	exitcode = yyparse();
	buffer_index=0;
	printvals(sym_table);
    }
    return exitcode;
}
