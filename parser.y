%{
    #include "parser.h"
    int n_inputs = 0;
    int buffer_index = 0;
%}

/* %parse-param {char* statement} */

%define api.value.type union
%token <int> NUM
%token <inputrec *> VAR
%nterm <int> exp

/* %precedence '=' */
%left '|' '+'
%left '&' '.'
%left '?'
%left '^'
%precedence NEG

%%

input:
     %empty
| input line
;

line:
    '\n'	{}
| exp '\n'	{result = $1;}
| error '\n'	{yyerrok;}
;

exp:
   NUM
| VAR		{ $$ = $1->value;}
/* | VAR '=' exp	{ $$ = $3; $1->value = $3;} */
| exp '|' exp	{ $$ = $1 || $3; }
| exp '+' exp	{ $$ = $1 || $3; }
/* | exp exp	{ $$ = $1 && $2; } */
| exp '&' exp	{ $$ = $1 && $3; }
| exp '.' exp	{ $$ = $1 && $3; }
| exp '?' exp	{ $$ = !($1 && $3); }
| exp '^' exp	{ $$ = $1 ^ $3; }
| exp '@' exp	{ $$ = !$1 || $3; }
/* | exp "⊕" exp	{ $$ = $1 ^ $3; } */
| '!' exp %prec NEG { $$ = !$2; }
| "¬" exp %prec NEG { $$ = !$2; }
| '(' exp ')'	{ $$ = $2; }
;

%%


inputrec* sym_table;

inputrec*
putsym(char const *name)
{
    n_inputs++;
    inputrec *res = (inputrec *) malloc (sizeof(inputrec));
    res->name = strdup(name);
    res->next = sym_table;
    res->value = 0;
    sym_table = res;
    return res;
}

inputrec *
getsym (char const *name)
{
    for (inputrec *p = sym_table; p; p = p->next)
	if (strcmp (p->name, name) == 0)
	    return p;
    return NULL;
}

int getnumfrombuffer(void)
{
    if (!isdigit(buffer[buffer_index]))
	return -1;

    int val = 0;
    while(isdigit(buffer[buffer_index]))
    {
	val *= 10;
	val += buffer[buffer_index];
	buffer_index++;
    }
    return val;
}


int
yylex(void)
{
    int c = buffer[buffer_index++];
    while (c == ' ' || c == '\t')
	c = buffer[buffer_index++];

    if (c == EOF || c == '\0')
	return YYEOF;

    if (isdigit(c))
    {
	buffer_index--;
	/* if (scanf (buffer+buffer_index, "%d", &yylval.NUM ) != 1) */
	yylval.NUM = getnumfrombuffer();
	if (yylval.NUM == -1)
	    abort();
	return NUM;
    }

    if (isalpha(c))
    {
	static ptrdiff_t bufsize = 0;
	static char *symbuf = 0;
	ptrdiff_t i = 0;
	do
	{
	    // This is why std::string exists.
	    if (bufsize <=i)
	    {
		bufsize=2*bufsize+42;
		symbuf=realloc(symbuf, (size_t) bufsize);
	    }
	    symbuf[i++] = (char)c;
	    c=buffer[buffer_index++];
	} while (isalnum(c));

	buffer_index--;
	symbuf[i] = '\0';

	inputrec *s = getsym (symbuf);
	if (s == NULL)
	{
	    s = putsym(symbuf);
	}
	yylval.VAR = s;
	return VAR;
    }
    return c;

}

void yyerror(char const *s)
{
    fprintf(stderr, "%s\n", s);
}
